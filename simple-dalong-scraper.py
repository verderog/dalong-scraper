#!/usr/bin/env python

#
# This scraper scrapes Gundam data from the dalong.net site.  It will export
# a PDF file containing a grade set's landing page and the info for
# every Gundam linked by that page.  Currently (4/10/2022), for example,
# there are 244 Gundam listed at http://www.dalong.net/reviews/bf/bf_cata_e.htm.
# This results in a 60MB+ document 1700+ pages in length.
#
# Example invocation
# ------------------
# 	./simple-dalong-scraper.py --url http://www.dalong.net/reviews/pg/pg_cata_e.htm --output dalong-cat-pg.pdf
#
# How do you get the url?
# -----------------------
#
#	- go to http://www.dalong.net
#	- click on the grade set at the top to bring up that page.  For example, the "HG" link
#	- hold down the ctrl-button and click on the "English Catalog" link to break out of frame mode
#   - copy the URL.
#   - invoke the scraper using the copied URL and give it a unique PDF Output file name
#
import pdfkit
from bs4 import BeautifulSoup
import sys
import requests
import os
import argparse
import tempfile
from PyPDF2 import PdfFileMerger

# overrides for wkhtmltopdf to limit the amount of whitespace that gets
# introduced on various pages.
options = {
	"user-style-sheet": "css-tweaks.css"
}

parser = argparse.ArgumentParser(description="Simple scraper to pull Gundam pages from dalong.net and export to PDF")
parser.add_argument('--url',help="URL to parse",type=str,default='http://www.dalong.net/reviews/rg/rg_cata_e.htm')
parser.add_argument('--output',help="Base PDF file name to output data to",type=str,default="dalong_gundam_catalog.pdf")
parser.add_argument('--debug',help="Enable debug info",action="store_true")
parser.add_argument('--skip-pdf',help="Disable PDF output for testing",action="store_true")
parser.add_argument('--strict',help="Strict URL checking at the price of taking longer.",action="store_true")
args = parser.parse_args()

links = [s.strip() for s in args.url.split(",")]


def main():
	gundam_url_list = []
	
	for link in links:
		print("Parsing: " + link)
		page = requests.get(link)
		soup = BeautifulSoup(page.content,"html.parser")
		
		gundam_url_list.append(link)

		local_url_path = os.path.dirname(link)
		
		hrefs = soup.find_all('a')
		
		gundam_count = 0
		for href in hrefs:
			imgs = href.find_all('img')
			# if this has exactly one image associated with it
			if len(imgs) == 1:
				gundam_count += 1
				# convert URL _p (product review) to _i (information)
				if not("_p.htm" in href['href']):
					print("\tNo review page for: " + href['href'] + " using default...")
					gundam_url_list.append(local_url_path + "/" + href['href'])
				else:
					# use general naming convention dalong incorporates on his website
					check_url = local_url_path + "/" + href['href'].replace("_p.htm","_i.htm")
					
					if args.strict:
						# validate the URL actually exists.  It might not be present!
						response = requests.head(check_url)
						if response.status_code == 200:					
							gundam_url_list.append(check_url)
						else:
							print("\tInfo page not found.  Using default instead: " + href['href'] + ", code: " + str(response.status_code))
							gundam_url_list.append(local_url_path + "/" + href['href'])
					else:
						# no check.  Assume naming convention is correct.  May throw an error when PDF is created
						gundam_url_list.append(check_url)
						
			elif len(imgs) > 1:
				print("UNKNOWN: " + href['href'])
			else:
				if "href" in href.attrs:
					print("SKIPPED: " + href['href'])
				else:
					print("SKIPPED: " + href.prettify())
				
		print("Gundam count: " + str(gundam_count))
	
	if args.debug:
		for url in gundam_url_list:
			print(url)

	if not args.skip_pdf:
		print("Exporting PDF.  NOTE: This may take a while to complete...")
		
		# partition the list because wkhtmltopdf cannot handle a huge URL set
		list_position = 0
		PARTITION_SIZE = 128
		
		with tempfile.TemporaryDirectory() as tmpdirname:
			merger = PdfFileMerger()
			while list_position < len(gundam_url_list):
				tmpfile_name = tmpdirname + "/" + str(list_position) + ".pdf"
				
				if args.debug:
					print("Writing tmp file: " + tmpfile_name) 
					
				pdfkit.from_url(gundam_url_list[list_position:(list_position+PARTITION_SIZE)],tmpfile_name,options=options,verbose=True)
				list_position += PARTITION_SIZE
				merger.append(tmpfile_name)
				
			# perform the PDF merge
			merger.write(args.output)
			merger.close()

	print("Done!")
	
if __name__ == "__main__":
	sys.exit(main())
