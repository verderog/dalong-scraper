#!/usr/bin/env python

import requests
import os
from bs4 import BeautifulSoup
import sys
import pdfkit
import argparse
import tempfile
from PyPDF2 import PdfFileMerger
import time
import re
import regex
import csv
import urllib.parse

# Top-level URL
URL = "http://www.dalong.net/"
OUTPUT_FILE = "dalong-gundam-catalog-"

#
# Can't auto-discover easily since some of the dalong html code is
# broken.  Use a pre-defined list.  Links not in this list or
# the ignore list will produce a warning so that future web site updates
# will at least be shown.
#
GRADE_LIST = {
	"mg1.htm":"MG",
	"mg2.htm":"MG2",
	"rg.htm":"RG",
	"hg.htm":"HG",
	"pg.htm":"PG",
	"sd.htm":"SD",
	"sd2.htm":"SD2",
	"seed.htm":"SEED",
	"00.htm":"00",
	"age.htm":"AGE",
	"bf.htm":"BUILD",
	"ib.htm":"IBO",
	"getc.htm":"ETC-G",
	"cg.htm":"CLUB-G",
	"old.htm":"OLD",
	"metc.htm":"ETC-M",
	"fetc.htm":"ETC-F",
	"sw.htm":"SW",
	"mkoto.htm":"KOTO-M",
	"fkoto.htm":"KOTO-F",
}

#
# Top-level html ignore list when processing grade links from the dalong site
#
IGNORE_HTM_LIST = [
	"1stpage.htm",
	"http://cafe.naver.com/dalongnet/",
	"gal.htm",
	"../link.htm",
	"../dalong.htm",
]

#
# List of ignored html fragments when creating scrape list.  Note that "_cata" is included as a separate
# operation outside of referencing this list.
#
SCRAPE_IGNORE_LIST = [
	"_list",
	"_cata",
	"mgdata",
]

# overrides for wkhtmltopdf to limit the amount of whitespace that gets
# introduced on various pages.
options = {
	"user-style-sheet": "css-tweaks.css"
}


parser = argparse.ArgumentParser(description="Scraper for pulling Gundam pages from dalong.net and exporting to PDF")
parser.add_argument('--output',help="File to output data to (default: dalong-gundam-catalog-<set-name or multi>.(pdf|csv))",type=str)
parser.add_argument('--debug',help="Enable debug info log level. 0=none (default), >=1 for increasing levels.",type=str,default="0")
#parser.add_argument('--strict',help="Strict URL checking at the price of taking longer.",action="store_true")
parser.add_argument('--grade-sets',help="Comma-separated grades set to scrape (MG/MG2/RG/HG/PG/SD/SD2/SEED/OO/AGE/BUILD/IBO/ETG-G/CLUB-G/OLD/ETC-M/ETC-F/SW/KOTO-M/KOTO-F/ALL).",type=str,default="ALL")
parser.add_argument('--language',help="Select English (default) or Korean output",type=str,default="english")
parser.add_argument('--top-level-only',help="Scrape only 'top-level' catalog pages",action="store_true")
parser.add_argument('--verbose',help="Print top-level operations as they are performed",action="store_true")
#parser.add_argument('--extract-manual',help="Extract scanned manuals to pdf",action="store_true")
parser.add_argument('--create-csv',help="Generate Gunpla CSV checklist file",action="store_true")
parser.add_argument('--create-pdf',help="Generate Gunpla PDF file",action="store_true")
parser.add_argument('--skip-output',help="Do not generate output file",action="store_true")
parser.add_argument('--impatient',help="Remove 'niceness' delay for retrieving data from dalong site",action="store_true")
parser.add_argument('--debug-dalong-code',help="Reference only Gundam matching the debug code.  Debug purposes.",type=str)
args = parser.parse_args()

args.language = args.language.upper()
requested_grade_sets = [s.strip().upper() for s in args.grade_sets.split(",")]


########################################################################
#
# Parse root page for the header info, which contains the top-level
# link for the grade sets.
#
########################################################################
def get_grade_sets_url(debug_level=0):
	if args.verbose:
		print("Retrieving grade sets url...")
		
	page = requests.get(URL)

	#print(page.text)
	soup = BeautifulSoup(page.content,"html.parser")

	#
	# Content is provided within a frame
	#
	frames = soup.find_all("frame")

	#
	# Search the frames for the 'header', which has the all of the links to
	# every grade-set.
	#

	grade_set_url = ''

	for frame in frames:
		if ('name' in frame.attrs) and (frame['name'] == 'header'):
			grade_set_url = URL + frame['src']
			break

	return grade_set_url
	
########################################################################
#
# Parse the grade set URLs.  Some of the HTML is messed up, so use
# a predefined array gathered from dalong to compensate.  Include an
# ignore list so that *new* entries are identified if they are detected
# by this parser.
#
########################################################################
def parse_grade_sets_url(grade_set_url,debug_level=0):
	page = requests.get(grade_set_url)
	soup = BeautifulSoup(page.content,"html.parser")

	grade_urls = {}

	if args.verbose:
		print("Processing all grade sets...")

	#
	# Grade set URLs are held within a table.  Parse this table to extract
	# a list of grade-sets to eventually traverse.
	#
	links = soup.find_all('a')
	for link in links:
		if "href" in link.attrs:
			link_text=link['href']
		else:
			# no href field... skip
			continue
			
		if link_text in GRADE_LIST:
			if ("ALL" in requested_grade_sets) or (GRADE_LIST[link_text] in requested_grade_sets):
				grade_urls[link_text]={"base_url":os.path.dirname(grade_set_url)+"/"+link['href'],"set_name":GRADE_LIST[link_text]}
			else:
				pass # not a requested set...
		elif link_text in IGNORE_HTM_LIST:
			pass
		else:
			# omit any empty text
			if (not link_text) and (debug_level > 0):
				print("UNKNOWN, SKIPPING: " + link_text)

	if debug_level >= 10:
		print(grade_urls)
		
	return grade_urls

########################################################################
#
# Retrieve top-level Korean grade set links
#
########################################################################
def retrieve_all_korean_grade_set_links(grade_urls,debug_level=0):
	if args.verbose:
		print("Processing available Korean grade sets...")

	for key in grade_urls:
		url = grade_urls[key]["base_url"]
		if debug_level >= 10:
			print("\tSet:" + GRADE_LIST[key] + "(" + url + ")")

		page=requests.get(url)
		soup=BeautifulSoup(page.content,"html.parser")
		
		# Get current context based on the frame
		frames = soup.find_all("frame")
		
		if 'src' in frames[0].attrs:
			grade_urls[key]["korean_grade_url_list"] = os.path.dirname(url) + "/" + os.path.dirname(frames[0]['src']) + "/" + os.path.basename(frames[0]['src'])
		else:
			if debug_level >= 10:
				print("Korean 'list' not found!")
			
		if 'src' in frames[1].attrs:
			grade_urls[key]["korean_grade_url_main"] = os.path.dirname(url) + "/" + os.path.dirname(frames[1]['src']) + "/" + os.path.basename(frames[1]['src'])
		else:
			if debug_level >= 10:
				print("Korean 'main' not found!")

	if debug_level >= 20:
		print(grade_urls)
		
	return grade_urls
		
########################################################################
#
# Retrieve top-level English grade set links
#
########################################################################
def retrieve_all_english_grade_set_links(grade_urls,debug_level=0):
	if args.verbose:
		print("Processing available English grade sets...")
		
	for key in grade_urls:
		#
		# "List" menu search
		#
		if not args.top_level_only:
			url = grade_urls[key]["korean_grade_url_list"]
			page=requests.get(url)
			soup=BeautifulSoup(page.content,"html.parser")
			
			if debug_level >= 10:
				print("\tSet:" + GRADE_LIST[key] + "(" + url + ")")


			# Search links for the English catalog
			links=soup.find_all('a')
			cata_e = ''
			for link in links:
				if ('href' in link.attrs) and ("_list" in link['href']):
					link_text=''.join(link.getText().splitlines()) # text might come back split.  Correct this
					link_text=''.join(c for c in link_text if c.isascii()) # text might contain strange characters.  Nuke those.
					if "english menu" in link_text.lower():
						cata_e = os.path.basename(link['href'])
						break

			if cata_e == '':
				print("Could not find English menu list URL for: " + url)
			else:
				grade_urls[key]["english_grade_url_list"] = os.path.dirname(url) + "/" + cata_e
			
		#
		# "Main" menu search
		#
		url = grade_urls[key]["korean_grade_url_main"]
		page=requests.get(url)
		soup=BeautifulSoup(page.content,"html.parser")

		# Search links for the English catalog
		links=soup.find_all('a')
		cata_e = ''
		for link in links:
			if ('href' in link.attrs) and ("_cata" in link['href']):
				link_text=''.join(link.getText().splitlines()) # text might come back split.  Correct this
				link_text=''.join(c for c in link_text if (c.isascii() and c.isprintable())) # text might contain strange characters.  Nuke those.
				if "english catalog" in link_text.lower():
					cata_e = os.path.basename(link['href'])
					break

		if cata_e == '':
			print("Could not find English catalog URL for: " + url)
		else:
			grade_urls[key]["english_grade_url_main"] = os.path.dirname(url) + "/" + cata_e

	return grade_urls
	
########################################################################
#
# Iterate through each set and store the kit info for each one.
#
########################################################################
def create_scrape_list(grade_urls,debug_level=0):
	if args.verbose:
		print("Defining scrape list...")
		
	scrape_list = []
	for key in grade_urls:
		if debug_level >= 10:
			print("\tSet:" + GRADE_LIST[key])

		#
		# add the top-level main "catalog".  If english is present, then
		# go ahead and use that (will not be added for Korean, so no point
		# to check the args...)
		#
		if "english_grade_url_main" in grade_urls[key]:
			catalog_url = grade_urls[key]["english_grade_url_main"]
		else:
			if args.language == "ENGLISH":
				print("\t\tEnglish catalog not present, substituting with Korean ...")
				#catalog_url = "https://www-dalong-net.translate.goog/" + "/".join(grade_urls[key]["korean_grade_url_main"].split("/")[3:]) + "?_x_tr_sch=http&_x_tr_sl=ko&_x_tr_tl=en&_x_tr_hl=en"
				catalog_url = grade_urls[key]["korean_grade_url_main"]
			
		# do not include top-level page for CSV creation (these pages are unused for CSV)
		if not args.create_csv:
			scrape_list.append(catalog_url)
			
		# skip the remainder if only gathering top-level info...
		if (args.top_level_only):
			continue
		
		if "english_grade_url_list" in grade_urls[key]:
			list_url = grade_urls[key]["english_grade_url_list"]
		else:
			list_url = grade_urls[key]["korean_grade_url_list"]
			if args.language == "ENGLISH":
				print("\t\tEnglish catalog not present, substituting Korean...")

		if debug_level >= 20:
			print("\tParsing: " + list_url)
		
		page = requests.get(list_url)
		soup = BeautifulSoup(page.content,"html.parser")
		
		gundam_url_base_path = os.path.dirname(list_url)
		
		links = soup.find_all('a')

		gundam_count = 0
		
		for link in links:
			if "href" in link.attrs:
				#if ("_list" in link['href']) or ("_cata" in link['href']):				
				skip_url = False
				for ignore in SCRAPE_IGNORE_LIST:
					if ignore in link['href']:
						skip_url = True
						break
						
				if skip_url:
					continue
				else:
					url = gundam_url_base_path + "/" + link['href']

					# Convert relative URLs to absolute path to prevent duplicates
					if '..' in url:
						url = absolutifyrelativeurl(url)
					
					# if the url isn't already present in the list (due to bad HTML code)...
					if not (url in scrape_list):
						gundam_count += 1
						scrape_list.append(url)
						if debug_level >= 30:
							link_text = link.get_text()
							link_text=''.join(c for c in link_text if c.isprintable()) # text might contain strange characters or newlines.  Nuke those.
							if not (not link_text):
								print("\t" + link_text) # condense multilines...

		grade_urls[key]['gundam_count'] = gundam_count
		if debug_level >= 10:
			print("\tGundam count: " + str(gundam_count))
		
	if debug_level >= 40:
		print(scrape_list)
		
	return scrape_list

########################################################################
#
# Iterate through the gundam list and export everything to PDF. This is a slow process.
#
########################################################################
def create_pdf_from_list(gundam_url_list,debug_level=0):	
	if not args.skip_output:

		if not args.output:
			filename = (OUTPUT_FILE + '-'.join(requested_grade_sets) + ".pdf").lower()
		else:
			filename = args.output
			
		print("Exporting PDF to " + filename + ".  NOTE: This may take a while to complete...")
			
		if debug_level >= 10:
			print("Total urls to process into PDF: " + str(len(gundam_url_list)))
		
		# partition the list because wkhtmltopdf cannot handle a huge URL set
		list_position = 0
		PARTITION_SIZE = 16
		
		with tempfile.TemporaryDirectory() as tmpdirname:
			merger = PdfFileMerger()
			while list_position < len(gundam_url_list):
				tmpfile_name = tmpdirname + "/" + str(list_position) + ".pdf"
				print("Completion: " + str(int(list_position/PARTITION_SIZE)) + " / " + str(int(len(gundam_url_list)/PARTITION_SIZE)))
				if debug_level > 10:
					print("Writing tmp file: " + tmpfile_name) 
					print(gundam_url_list[list_position:(list_position+PARTITION_SIZE)])
					
				pdfkit.from_url(gundam_url_list[list_position:(list_position+PARTITION_SIZE)],tmpfile_name,options=options,verbose=(debug_level > 0))
				list_position += PARTITION_SIZE
				merger.append(tmpfile_name)
				
			# ensure for display purposes user sees X/X for completion (happens if count is an exact multiple of the partition size
			if list_position == len(gundam_url_list):
				print("Completion: " + str(PARTITION_SIZE) + " / " + str(PARTITION_SIZE))
				
			if debug_level > 0:
				print("Finalizing PDF...")
				
			# perform the PDF merge
			merger.write(filename)
			merger.close()	

########################################################################
#
# Iterate through the gundam list, detect manual pages, and scrape them.
#
########################################################################
def extract_manual_from_list(gundam_url_list,debug_level=0):
	if not args.skip_output:
		
		# define manual PDF name based on set name and set
		# if manual isn't already present
		# 	search each PDF for "manual" entry
		pass
		
	else:
		for url in gundam_url_list:
			manual_urls = []
			print("Parsing: " + url)
			
			parent_url = get_url_parent(url)
			basename = os.path.basename(parent_url)
			prog = re.compile("[0-9]{4}")
					
			page = requests.get(url)
			soup = BeautifulSoup(page.content,"html.parser")
			
			codename = soup.find('td',class_='CodeName')
			fullname = soup.find('td',class_='FullName')
			
			print("\tcodename: " + get_text_filtered(codename))
			print("\tfullname: " + get_text_filtered(fullname))

			links = soup.find_all('a')
			
			for link in links:
				if 'href' in link.attrs:
					if 'jpg' in link['href'].lower():
						if prog.search(os.path.basename(link['href'])):
							manual_urls.append(parent_url + "/" + link['href'])

			if len(manual_urls) == 0:
				print("\t*** No manual pages found")
			else:
				print("\tFound: " + str(len(manual_urls)) + " pages")
				
			if not args.impatient:
				time.sleep(1)
			
########################################################################
#
# Iterate through the gundam list, detect manual pages, and scrape them.
#
########################################################################
def create_csv_from_list(gundam_url_list,debug_level=0):
	gundam_dict_array = []
	gundam_csv_fields = ['grade_set_name','dalong_code_name','codename','fullname','release_date','built','backlog','url']
	
	for url in gundam_url_list:
		parent_url = get_url_parent(url)
		basename = get_basename(parent_url)

		dalong_code_name = basename
		
		if args.debug_dalong_code and dalong_code_name != args.debug_dalong_code:
			continue

		if args.verbose:
			print("Parsing: " + basename + " (" + url + ")")
						
		page = requests.get(url)
		soup = BeautifulSoup(page.content,"html.parser")
		
		topline = soup.findAll("tr", {"class" : re.compile('TopLine')})
		
		if not topline:
			if args.verbose:
				print("\t*** No topline found: (" + url + "). Ignoring")
			continue
			
		if len(topline) > 1:
			if debug_level > 0:
				print("\t*** Multiple tr's found.  Ignoring.")
			continue

		#
		# Parse the dalong page's header which may include grade set, codename, and fullname
		#

		tdvals = topline[0].findAll("td",recursive=False)
		first_entry = True
		gundam_dict = {"url":url,"dalong_code_name":dalong_code_name}
		for td in tdvals:
			# first entry is unlabeled and corresponds to grade
			if first_entry:
				gundam_dict["grade_set_name"] = get_text_filtered(td)
				first_entry = False
			else:
				if "class" in td.attrs:
					if td["class"][0] == "CodeName":
						gundam_dict["codename"] = get_text_filtered(td)
					elif td["class"][0] == "FullName":
						gundam_dict["fullname"] = get_text_filtered(td)
		
		#release_date_search_pattern = "(발매일)(.*)\:(.*)+(19[0-9]{2}\.[0-9]{1,2}|20[0-9]{2}\.[0-9]{1,2})"
		release_date_search_pattern = "발매일.*\: +(.*)"
		release_date_text = soup.body.find(text=re.compile(release_date_search_pattern))
		if not (not release_date_text):
			release_date_search = re.search(release_date_search_pattern,release_date_text)
			if release_date_search.lastindex == 1:
				gundam_dict["release_date"] = remove_all_whitespace(release_date_search.group(1))
		
		gundam_dict_array.append(gundam_dict)
		if debug_level >= 10:
			print(gundam_dict)

		if not args.impatient:
			time.sleep(0.5)	
		
	if not args.output:
		filename = (OUTPUT_FILE + '-'.join(requested_grade_sets) + ".csv").lower()
	else:
		filename = args.output
		
	if not args.skip_output:
		if args.verbose:
			print("Outputting CSV file...")
		with open(filename,'w',newline='') as csvfile:
			writer = csv.DictWriter(csvfile,fieldnames=gundam_csv_fields) # extrasaction="ignore" to ignore keys not defined
			writer.writeheader()
			for g in gundam_dict_array:
				writer.writerow(g)

def absolutifyrelativeurl(url):
	url_rel = url.split('..')
	base = url_rel[0]
	for item in url_rel[1:]:
		base = urllib.parse.urljoin(base,".." + item)
	return base


def get_basename(url):
	return os.path.basename(url)

def get_debug_level(debug_input,debug_filter):
	return int(debug_input,16) & 0xFF if (int(args.debug,16) & debug_filter) else 0

def get_url_parent(url):
	return os.path.dirname(url)
	
def get_text_filtered(soup_tag):
	if not (not soup_tag):
		soup_text = soup_tag.get_text()
		if not (not soup_text):
			return remove_korean_chars_and_extra_whitespace(soup_text)
	return ''
	
def remove_korean_chars_and_extra_whitespace(text):
	text = " ".join(text.split()) # remove sequential white space (takes care of nbsp)
	text = ''.join(c for c in text if ((not is_hangul(c)) and c.isprintable())) # text might contain strange characters or newlines, and leading / trailing space.  Nuke those.
	text = " ".join(text.split()) # remove sequential white space after koren removal
	return text

def remove_extra_whitespace(text):
	text = " ".join(text.split()) # remove double-up space
	return text

def remove_all_whitespace(text):
	text = "".join(text.split()) # remove double-up space
	return text

def is_hangul(value):
    if regex.search(r'\p{IsHangul}', value):
        return True
    return False
		
########################################################################
#
# main function
#
########################################################################
if __name__ == "__main__":
	
	if (not args.create_csv) and (not args.create_pdf):
		print("Nothing to do.  Please specify '--create-csv' or '--create-pdf'.  Terminating...")
		sys.exit(-1)
		
	grade_set_url = get_grade_sets_url(debug_level=get_debug_level(args.debug,0x100))
	if not grade_set_url:
		print("Could not locate grade sets url!  Terminating...")
		sys.exit(-1)
		
	grade_urls = parse_grade_sets_url(grade_set_url,debug_level=get_debug_level(args.debug,0x200))
	if not grade_urls:
		print("No grade sets parsed!  Terminating...")
		sys.exit(-1)
		
	grade_urls = retrieve_all_korean_grade_set_links(grade_urls,debug_level=get_debug_level(args.debug,0x400))
	if not grade_urls:
		print("Korean sets not found!  Terminating...")
		sys.exit(-1)
		
	if args.language == "ENGLISH":
		grade_urls = retrieve_all_english_grade_set_links(grade_urls,debug_level=get_debug_level(args.debug,0x800))
	
	scrape_list = create_scrape_list(grade_urls,debug_level=get_debug_level(args.debug,0x1000))
	if not scrape_list:
		print("Nothing to scrape.  Terminating...")
		sys.exit(-1)
		
	if args.create_csv:
		create_csv_from_list(scrape_list,debug_level=get_debug_level(args.debug,0x8000))
	elif args.create_pdf:
		create_pdf_from_list(scrape_list,debug_level=get_debug_level(args.debug,0x2000))
	
	print("Done!")
	
	
