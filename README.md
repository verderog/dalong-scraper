# Gundam Scraper

This scraper scrapes Gundam data from the dalong.net site.  It includes
options to export web pages to a PDF file and for creating a comma-separated
value file with model information.

## PDF

### Top-level page and info pages

A PDF file containing a grade set's top-level page and the info for
every Gundam linked by that page can be created.  The top-level includes
all the Gundam for a particular grade, and the info related to each
listed Gundam (such as box art, manual scans, and runner images.)

### Top-level only

A PDF file containing the top-level landing page for grade sets can
also be exported.  This does not include the info pages.

### General

The images in the resulting PDFs are on the small side but  contain
links back to the dalong.net site.  Assuming your PDF reader supports it,
hover your cursor over the bottom of each image to bring up the link to
click on.

This helps serve as an off-line reference that can be annotated with 
various PDF editors that are available.

## CSV

A CSV of the desired sets can be created.  This file can then be
imported into a spreadsheet or database for tracking your collection.  Fields
are included for "built" and "backlog".


# Requirements

## Python packages

### pdfkit

### BeautifulSoup

### requests

### PyPDF2

## Ubuntu Focal (20.04) packages

### wkhtmltopdf 

The currently apt-get available release of wkhtmltopdf does not support
multiple URLs to PDF pages.  Perform the steps below to get a version 
that includes this support.

- (optional) `sudo apt-get remove wkhtmltopdf` to remove any previous installs 
- `wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.focal_amd64.deb`
- `dpkg --install wkhtmltox_0.12.6-1.focal_amd64.deb`.  This may fail with missing dependencies.  Used `sudo apt-get install` to install these dependencies

# Help

Execute `./dalong-gundam-scraper.py --help` to see available options.

# Recipes

## PDF

### Single grade set and all info pages for that grade set

`./dalong-gundam-scraper.py --create-pdf --verbose --grade-sets PG`

Resulting `dalong-gundam-catalog-pg.pdf` will be created.

### Selective grade sets info

`./dalong-gundam-scraper.py --create-pdf --verbose --grade-sets PG,RG`

Resulting `dalong-gundam-catalog-pg-rg.pdf` will be created.

### All grade sets and all info pages for all grade sets

`./dalong-gundam-scraper.py --create-pdf --verbose`

Resulting `dalong-gundam-catalog-all.pdf` will be created.

### Top-level only for a single grade set

`./dalong-gundam-scraper.py --create-pdf --verbose --grade-sets PG --top-level-only --output tl-pg.pdf`

Resulting `tl-pg.pdf` will be created.

### Top-level only for selective grade sets

`./dalong-gundam-scraper.py --create-pdf --verbose --grade-sets PG,RG --top-level-only --output tl-pg-rg.pdf`

Resulting `tl-pg-rg.pdf` will be created.

### Top-level for all grade sets

`./dalong-gundam-scraper.py --create-pdf --verbose --top-level-only --output tl-all.pdf`

Resulting `tl-all.pdf` will be created.

## CSV

### Single grade set listing

`./dalong-gundam-scraper.py --create-csv --verbose --grade-sets PG`

Resulting `dalong-gundam-catalog-pg.csv` will be created.

### Selective grade sets listing

`./dalong-gundam-scraper.py --create-csv --verbose --grade-sets PG,RG`

Resulting `dalong-gundam-catalog-pg-rg.csv` will be created.

### Entire grade set listing

`./dalong-gundam-scraper.py --create-csv --verbose`

Resulting `dalong-gundam-catalog-all.csv` will be created.
